from django.db import models, DataError
from django.conf import settings
from django.urls import reverse

from .rename import avatar_rename, cover_rename


class GenreVO(models.Model):
    name = models.CharField(max_length=150, unique=True)
    import_href = models.CharField(max_length=150)

    def __str__(self):
        return self.name


class UserProfile(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, related_name="profile", on_delete=models.CASCADE
    )
    pronouns = models.CharField(max_length=50, blank=True)
    bio = models.TextField(blank=True)

    # allows the user to choose many or none favorite genres
    favorite_genre = models.ManyToManyField(
        GenreVO, related_name="profiles", blank=True
    )

    # allows the user to update their avatar/cover image by upload or url
    avatar_url = models.URLField(null=True, blank=True)
    avatar_file = models.ImageField(null=True, blank=True, upload_to=avatar_rename)
    cover_url = models.URLField(null=True, blank=True)
    cover_file = models.ImageField(null=True, blank=True, upload_to=cover_rename)

    def save(self, *args, **kwargs):
        if self.avatar_url is None and self.avatar_file is None:
            raise DataError("avatar_url and avatar_file cannot both be null")
        if self.cover_url is None and self.cover_file is None:
            raise DataError("cover_url and cover_file cannot both be null")
        super().save(*args, **kwargs)

    @property
    def avatar(self):
        if self.avatar_file is not None:
            return self.avatar_file.url
        else:
            return self.avatar_url

    @property
    def cover(self):
        if self.cover_file is not None:
            return self.cover_file.url
        else:
            return self.cover_url

    def __str__(self):
        return f"{self.user.username}'s Profile"

    def get_api_url(self):
        return reverse("api_profile_detail", kwargs={"user_id": self.user.id})
