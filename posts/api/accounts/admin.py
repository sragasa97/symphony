from django.contrib import admin

from .models import GenreVO, UserProfile


@admin.register(GenreVO)
class GenreVOAdmin(admin.ModelAdmin):
    pass


@admin.register(UserProfile)
class UserProfileAdmin(admin.ModelAdmin):
    pass
