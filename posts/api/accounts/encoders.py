from common.modelencoder import ModelEncoder
from django.conf import settings

from .models import GenreVO, UserProfile


class GenreVOEncoder(ModelEncoder):
    model = GenreVO
    properties = ["import_href", "id", "name"]


class UserEncoder(ModelEncoder):
    model = settings.AUTH_USER_MODEL
    properties = ["username", "first_name", "last_name"]


class UserProfileEncoder(ModelEncoder):
    model = UserProfile
    properties = [
        "user",
        "pronouns",
        "bio",
        "favorite_genre",
        "avatar",
        "cover",
    ]

    encoders = {"user": UserEncoder(), "favorite_genre": GenreVOEncoder()}
