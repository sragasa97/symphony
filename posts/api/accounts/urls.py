from django.urls import path

from .views import api_profile, api_genreVO


urlpatterns = [
    path("profile/<int:user_id>/", api_profile, name="api_profile"),
    path("genres/", api_genreVO, name="api_genreVO"),
]
