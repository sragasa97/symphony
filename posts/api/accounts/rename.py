from django.utils.deconstruct import deconstructible
from uuid import uuid4
import os

"""
These classes are called when a user uploads their own avatar or cover image. It changes the original filename and formats it to provide uniformity in the database.

"""

# avatar
@deconstructible
class UploadPathAndRenameAvatar(object):
    def __init__(self, sub_path):
        self.path = sub_path

    def __call__(self, instance, filename):
        ext = filename.split(".")[-1]
        if instance.user.username:
            filename = f"{instance.user.username}_avatar.{ext}"
        else:
            # if username does not exist, replace with a random, unique 32 character hexadecimal string
            filename = f"{uuid4().hex()}_avatar.{ext}"
        return os.path.join(self.path, filename)


avatar_rename = UploadPathAndRenameAvatar("avatars/")


# cover
@deconstructible
class UploadPathAndRenameCover(object):
    def __init__(self, sub_path):
        self.path = sub_path

    def __call__(self, instance, filename):
        ext = filename.split(".")[-1]
        if instance.user.username:
            filename = f"{instance.user.username}_cover.{ext}"
        else:
            # if username does not exist, replace with a random, unique 32 character hexadecimal string
            filename = f"{uuid4().hex()}_cover.{ext}"
        return os.path.join(self.path, filename)


cover_rename = UploadPathAndRenameCover("covers/")
