from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

from .encoders import UserProfileEncoder, GenreVOEncoder
from .models import UserProfile, GenreVO

"""
Only the 'GET' and 'PUT' request methods for individual profiles are given view functions because:
1. It's unnecessary to grab a list of profiles of every user of the application.
2. Profiles are already made automatically through signals upon creation of a user.
3. Profiles should not have the capability to be deleted. They will be deleted with the deletion of a user.

"""


@require_http_methods(["GET"])
def api_genreVO(request):
    genres = GenreVO.objects.all()

    return JsonResponse({"genres": genres}, encoder=GenreVOEncoder)


@require_http_methods(["GET", "PUT"])
def api_profile(request, user_id):
    if request.method == "GET":
        profile = UserProfile.objects.get(user_id=user_id)

        return JsonResponse({"profile": profile}, encoder=UserProfileEncoder)
    else:
        content = json.loads(request.body)
        content["favorite_genre"] = []

        if "favorite_genres_request" in content:
            try:
                for selection in content["favorite_genres_request"]:
                    genre = GenreVO.objects.get(import_href=selection)
                    content["favorite_genre"].push(genre)
            except GenreVO.DoesNotExist:
                return JsonResponse(
                    {
                        "message": "Genre does not exist. Please indicate genre by import_href value."
                    }
                )

        del content["favorite_genres_request"]
        GenreVO.objects.filter(user_id=user_id).update(**content)

        profile = UserProfile.objects.get(user_id=user_id)
        return JsonResponse(profile, encoder=UserProfileEncoder, safe=False)
