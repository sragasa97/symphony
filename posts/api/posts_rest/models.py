from django.db import models
from django.conf import settings
from django.urls import reverse


class Post(models.Model):
    title = models.CharField(max_length=200, blank=True)
    body = models.TextField(blank=False)
    artist_id = models.CharField(max_length=250, blank=True)
    album_id = models.CharField(max_length=250, blank=True)
    track_id = models.CharField(max_length=250, blank=True)

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="posts",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.title

    def get_api_url(self):
        return reverse("api_post_detail", kwargs={"id": self.id})
