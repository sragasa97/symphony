from common.modelencoder import ModelEncoder
from django.conf import settings

from .models import Post


class UserEncoder(ModelEncoder):
    model = settings.AUTH_USER_MODEL
    properties = ["username", "first_name", "last_name"]


class PostEncoder(ModelEncoder):
    model = Post
    properties = ["href", "user", "title", "body", "artist_id", "album_id", "track_id"]

    encoders = {"user": UserEncoder()}
