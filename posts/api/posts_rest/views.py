from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

from .encoders import PostEncoder
from .models import Post


@require_http_methods(["GET", "POST"])
def api_posts(request):
    if request.method == "GET":
        posts = Post.objects.all()

        return JsonResponse({"posts": posts}, encoder=PostEncoder)
    else:
        content = json.loads(request.body)

        post = Post.objects.create(**content)
        return JsonResponse(post, encoder=PostEncoder, safe=False)


@require_http_methods(["GET", "PUT", "DELETE"])
def api_post_detail(request, id):
    if request.method == "GET":
        post = Post.objects.get(id=id)

        return JsonResponse({"post": post}, encoder=PostEncoder)
    elif request.method == "DELETE":
        count, items = Post.objects.filter(id=id).delete()

        return JsonResponse({"deleted": {"successful?": count > 0, "items": items}})
    else:
        content = json.loads(request.body)

        Post.objects.filter(id=id).update(**content)
        post = Post.objects.get(id=id)

        return JsonResponse({"post": post}, encoder=PostEncoder)
