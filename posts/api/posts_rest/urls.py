from django.urls import path

from .views import api_posts, api_post_detail


urlpatterns = [
    path("posts/", api_posts, name="api_posts"),
    path("posts/<int:id>/", api_post_detail, name="api_post_detail"),
]
