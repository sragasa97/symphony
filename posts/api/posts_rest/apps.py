from django.apps import AppConfig


class PostsRestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'posts_rest'
