import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "posts_project.settings")
django.setup()


from accounts.models import GenreVO


def get_genres():
    data = requests.get("http://spotify-api:8000/api/genres/")
    genres = json.loads(data.content)

    print(genres["genres"])

    for genre in genres["genres"]:
        GenreVO.objects.update_or_create(
            import_href=genre["href"],
            defaults={"id": genre["id"], "name": genre["name"]},
        )
        print("it worked")


def poll():
    while True:
        print("Post poller is polling for data!")
        try:
            get_genres()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(30)


if __name__ == "__main__":
    poll()
