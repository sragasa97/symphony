from dotenv import load_dotenv
import os
import base64
from requests import post
import json


load_dotenv()

client_id = os.getenv("CLIENT_ID")
client_secret = os.getenv("CLIENT_SECRET")

"""
The Client Credentials flow is used in server-to-server authentication. Since this flow does not include authorization, only endpoints that do not access user information can be accessed.

User requests access token -> Spotify accounts service returns access token -> User requests to Web API with access token in request -> Spotify web API returns requested (unscoped) data

"""


def get_token():
    auth_string = client_id + ":" + client_secret
    auth_bytes = auth_string.encode("utf-8")
    auth_base64 = str(base64.b64encode(auth_bytes), "utf-8")

    url = "https://accounts.spotify.com/api/token"
    headers = {
        "Authorization": "Basic " + auth_base64,
        "Content-Type": "application/x-www-form-urlencoded",
    }
    data = {"grant_type": "client_credentials"}

    response = post(url, headers=headers, data=data)

    try:
        result = json.loads(response.content)
        token = result["access_token"]
        return token
    except Exception as e:
        return {"message": "Error: ${e}"}


def get_auth_header(token):
    return {"Authorization": "Bearer " + token}


token = get_token()
