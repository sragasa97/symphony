from django.urls import path

from .views import api_genres, api_genre_detail


urlpatterns = [
    path("genres/", api_genres, name="api_genres"),
    path("genres/<str:name>/", api_genre_detail, name="api_genre_detail"),
]
