from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

from .encoders import GenreEncoder
from .models import Genre


@require_http_methods(["GET", "POST"])
def api_genres(request):
    if request.method == "GET":
        genres = Genre.objects.all()

        return JsonResponse({"genres": genres}, encoder=GenreEncoder)
    else:
        content = json.loads(request.body)

        genre = Genre.objects.create(**content)

        return JsonResponse(genre, encoder=GenreEncoder, safe=False)


# PUT request method is unnecessary because genres only have one attribute. Genres can just be deleted and made fresh.
@require_http_methods(["GET", "DELETE"])
def api_genre_detail(request, name):
    if request.method == "GET":
        genre = Genre.objects.get(name=name)

        return JsonResponse({"genre": genre}, encoder=GenreEncoder)
    else:
        count, items = Genre.objects.filter(name=name).delete()

        return JsonResponse({"deleted": {"successful?": count > 0, "items": items}})
