from django.db import models
from django.urls import reverse


class Genre(models.Model):
    name = models.CharField(max_length=150, unique=True, blank=False)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_genre_detail", kwargs={"name": self.name})
