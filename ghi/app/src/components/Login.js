import React from "react";
import styled from "styled-components";
import { CLIENT_ID } from "../keys";
import Logo from "../symphony_logo_with_text.png"


export default function Login() {

    const handleClick = () => {
        const clientID = CLIENT_ID;
        const redirectURL = "http://localhost:3000/";
        const apiURL = "https://accounts.spotify.com/authorize";
        const scope = [
            "user-read-email",
            "user-read-private",
            "user-modify-playback-state",
            "user-read-playback-state",
            "user-read-currently-playing",
            "user-read-recently-played",
            "user-read-playback-position",
            "user-top-read"
        ]
        window.location.href = `${apiURL}?client_id=${clientID}&redirect_uri=${redirectURL}&scope=${scope.join(" ")}&response_type=token&show_dialog=true`;
    }

    return (
    <Container>
        <img
            src={Logo}
            alt="Symphony Logo">
        </img>
        <button onClick={handleClick}>Connect to Spotify</button>
    </Container>
    );
}

const Container = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    height: 100vh;
    width: 100vw;
    background-color: #1db954;
    gap: 5rem;
    img {
        height: 20vh;
    }
    button {
        padding: 1rem 5rem;
        border-radius: 5rem;
        border: none;
        background-color: black;
        color: #49f585;
        font-size: 1.4rem;
        cursor: pointer
    }
`;
